.PHONY: all clean clean_data


all: data/covid19ru_rolling_weeks.csv

clean:
	rm -rf venv data

clean_data:
	rm -rf data

data/covid19_data_ru_government.csv: tools/get_governmental.py venv
	./venv/Scripts/python.exe $<

data/covid19_data_ru_government_with_additional_variables.csv: tools/calculate_additional_variables.py data/covid19_data_ru_government.csv
	./venv/Scripts/python.exe $<

data/covid19_data_ru_from_api.csv: tools/get_from_api.py venv
	./venv/Scripts/python.exe $<

data/population_data_ru_from_wiki.csv: tools/get_population_from_wiki.py
	./venv/Scripts/python.exe $<

data/geo_names_ru_all_sources.csv: tools/map_region_names.py data/covid19_data_ru_government.csv data/population_data_ru_from_wiki.csv
	./venv/Scripts/python.exe $<

data/gadm36_RUS_1_longitude_moved.rds: tools/get_shapes.R
	Rscript $<

data/covid19ru_final_dataset.csv: tools/creating_final_dataset.py data/geo_names_ru_all_sources.csv data/covid19_data_ru_government_with_additional_variables.csv
	./venv/Scripts/python.exe $<

data/covid19ru_rolling_weeks.csv: tools/calculate_rolling_weeks.py data/covid19ru_final_dataset.csv
	./venv/Scripts/python.exe $<

venv:
	python -m pip install -U --user virtualenv
	python -m virtualenv $@
	./$@/Scripts/python.exe -m pip install -r requirements.txt
