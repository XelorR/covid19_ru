# COVID-19, Russian data analysis

[Makefile](Makefile) based pipeline management example

## Sources used:

- https://стопкоронавирус.рф/information/
- http://covid19.bvn13.com/stats/all
- https://gadm.org/download_country_v3.html
- https://ru.wikipedia.org/wiki/Субъекты_Российской_Федерации
