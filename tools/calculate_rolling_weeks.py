# coding: utf-8

# %% [markdown]
# # Calculating rolling week sums
#
# This script will calculate last available 7 days sum and 7 days sum before last available 7 days
#
# ## Initializing

import pandas as pd

# %% [markdown]
# ## Reading covid19ru report

df = pd.read_csv("data/covid19ru_final_dataset.csv", parse_dates=["date"])
df.head()

# %% [markdown]
# ## Grouping weeks into separate dataframes
last_week = df.date.sort_values().unique()[-7:]
previous_week = df.date.sort_values().unique()[-14:-7]

df_last_week = df.loc[df["date"].isin(last_week)].groupby(
    ["FederalDistrict_RU", "FederalSubject_RU"], as_index=False
)[["sick_new", "healed_new", "died_new"]].sum()

df_previous_week = df.loc[df["date"].isin(previous_week)].groupby(
    ["FederalDistrict_RU", "FederalSubject_RU"], as_index=False
)[["sick_new", "healed_new", "died_new"]].sum()

# %% [markdown]
# ## Merging into one dataset

df_last_week["week"] = "last"
df_previous_week["week"] = "previous"
df_calculated = df_last_week.append(df_previous_week)
df_calculated

# %% [markdown]
# ## Saving to file

df_calculated.to_csv("data/covid19ru_rolling_weeks.csv", index=False)
