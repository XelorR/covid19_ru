# coding: utf-8

# %% [markdown]
# # Creating final dataset
#
# This script will merge covid19ru data with population data.
# Population, area and federal districts will be added.
#
# ## initialization

import pandas as pd

# %% [markdown]
# ## Reading all required files

covid19ru = pd.read_csv(
    "./data/covid19_data_ru_government_with_additional_variables.csv"
)
population = pd.read_csv("./data/population_data_ru_from_wiki.csv")
voc_geo = pd.read_csv("./data/geo_names_ru_all_sources.csv")


# %% [markdown]
# ## Unifying region names

population_with_unified_geo = population.merge(
    voc_geo, how="left", left_on="subject", right_on="NAME_1"
).drop(["NAME_1", "subject"], axis="columns")
covid19ru_with_unified_geo = covid19ru.merge(
    voc_geo, how="left", left_on="region_name", right_on="NAME_1"
).drop(["NAME_1", "region_name", "region_code"], axis="columns")

# %% [markdown]
# ## Merging covid19ru with population data
#
# active_share variable will be calculated too

covid19ru_final_dataset = covid19ru_with_unified_geo.merge(population_with_unified_geo)
covid19ru_final_dataset["active_share"] = (
    covid19ru_final_dataset["active"] / covid19ru_final_dataset["population"]
)

cols = ["date", "FederalDistrict_RU", "FederalSubject_RU"] + [
    c
    for c in covid19ru_final_dataset.columns
    if c not in ["date", "FederalSubject_RU", "FederalDistrict_RU"]
]

covid19ru_final_dataset = covid19ru_final_dataset[cols]
covid19ru_final_dataset

# %% [markdown]
# ## Saving results

covid19ru_final_dataset.to_csv("./data/covid19ru_final_dataset.csv", index=False)
covid19ru_final_dataset.to_excel("./data/covid19ru_final_dataset.xlsx", "covid19ru_data", index=False)