#!/usr/bin/env pyhton3
# coding: utf-8

import json
from urllib.request import urlopen
from pathlib import Path

import pandas as pd

json_data = urlopen("http://covid19.bvn13.com/stats/all").read().decode()
json_list = json.loads(json_data)['progress']

json_list[0].keys()

pd.read_json(json.dumps(json_list[0]["stats"]))

dataframe = pd.DataFrame()
for date in json_list:
    df = pd.DataFrame()
    df = pd.read_json(json.dumps(date["stats"]))
    df["datetime"] = date["datetime"]
    df["updatedOn"] = date["updatedOn"]
    dataframe = dataframe.append(df)

dataframe = dataframe[["datetime","sick","healed","died","region"]]
dataframe.columns = ["date","sick","healed","died","region_name"]
dataframe["date"] = dataframe["date"].apply(lambda x: x[:10])

p = Path("./data/")
if not p.exists():
    p.mkdir()


with open(p / "covid19_data_ru_from_api.json", "w") as j:
    j.write(json_data)

dataframe.to_csv(p / "covid19_data_ru_from_api.csv", index=False)