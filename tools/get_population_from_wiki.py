#!/usr/bin/env python3
# coding: utf-8

# %% [markdown]
# # Getting Russian population data
#
# from wikipedia
#
# ## Initialization
import re
from pathlib import Path

import pandas as pd

url = (
    "https://ru.wikipedia.org/wiki/%D0%A1%D1%83%D0%B1%D1%8A%D0%B5%D0%BA%D1%82%D1%8B_%D0%A0%D0%BE%D1%81%D1%81%D0%B8"
    "%D0%B9%D1%81%D0%BA%D0%BE%D0%B9_%D0%A4%D0%B5%D0%B4%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D0%B8#%D0%9F%D0%B5%D1%80%D0%B5"
    "%D1%87%D0%B5%D0%BD%D1%8C_%D1%81%D1%83%D0%B1%D1%8A%D0%B5%D0%BA%D1%82%D0%BE%D0%B2_%D0%A0%D0%BE%D1%81%D1%81%D0%B8"
    "%D0%B9%D1%81%D0%BA%D0%BE%D0%B9_%D0%A4%D0%B5%D0%B4%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D0%B8 "
)

# %% [markdown]
# ## Reading data from wikipedia

dfs = pd.read_html(url)

for df in dfs:
    try:
        if df.columns[1] == "Субъект Российской Федерации":
            wiki_table = df.copy()
            break
    except IndexError:
        pass

cols = [c for c in wiki_table.columns if ("Терри" in c) | ("Субъект" in c) | ("Насел" in c)]
wiki_table = wiki_table[cols].dropna()
wiki_table.columns = ["subject", "area_km_squared", "population"]
wiki_table = wiki_table.loc[wiki_table["subject"].str.contains("Российская") == False]
wiki_table["subject"] = wiki_table.subject.apply(lambda x: re.sub(r"[^А-Яа-яёЁ\- ]", r"", x).replace("  ", " "))
wiki_table["population"] = (wiki_table["population"].apply(lambda x: re.sub(r"[^0-9]", "", x)).astype("int"))
wiki_table.tail()

# %% [markdown]
# ## Saving results

p = Path("./data/")
if not p.exists():
    p.mkdir()

wiki_table.to_csv(p / "population_data_ru_from_wiki.csv", index=False)
