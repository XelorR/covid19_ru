#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# %%

from difflib import get_close_matches

import pandas as pd

# %% [markdown]
# ## reading data
voc = pd.read_csv("geo_names_ru.csv")
gov_data = pd.read_csv("data/covid19_data_ru_government.csv")
pop_data = pd.read_csv("data/population_data_ru_from_wiki.csv")

# %% [markdown]
# ## getting set of regions to recognize and voc to match
regions_to_recognize = set(gov_data.region_name.unique().tolist() +
                           pop_data.subject.unique().tolist())
unified_subjects = voc.FederalSubject_RU.unique().tolist()

# %% [markdown]
# ## generating dict of matched regions
generated_regions_voc_dict = {region: get_close_matches(region, unified_subjects, n=1)[0] for region in
                              regions_to_recognize}

# %% [markdown]
# ## making unified dataframe
generated_regions_voc_dataframe = pd.DataFrame({"NAME_1": list(regions_to_recognize)})
generated_regions_voc_dataframe["FederalSubject_RU"] = generated_regions_voc_dataframe["NAME_1"].map(
    generated_regions_voc_dict)
voc.tail()
voc = voc.append(
    generated_regions_voc_dataframe.merge(voc[["FederalSubject_RU", "FederalDistrict_RU"]].drop_duplicates(),
                                          how="left")).drop_duplicates()

voc

# %% [markdown]
# ## saving dataframe
voc.to_csv("data/geo_names_ru_all_sources.csv", index=False)